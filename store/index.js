import axios from 'axios'

export const state = () => ({
  url: 'http://myeverydayapps.com/public/_/items/',
  language: 2,
  projects: [],
  languages: [],
  projectsTranslations: []
})

export const getters = {
  projects(state) {
    const projects = []
    state.projects.forEach(project => {
      const translatedFields = project.translations.find(translation => translation.language_id === state.language);
      projects.push({ ...project, ...translatedFields });
    });
    return projects;
  }
}

export const mutations = {
  addToStore(state, token) {
    state[token.prop].push(...token.data);
  },
  changeLanguage(state, token) {
    state.language = token;
  }
}

export const actions = {
  async getInitialData({ state, commit }) {
    const projectsResponse = await axios(state.url + 'projects?fields=*.*');
    const projects = projectsResponse.data.data;
    const languagesResponse = await axios(state.url + 'languages');
    const languages = languagesResponse.data.data;
    commit('addToStore', { prop: 'projects', data: projects });
    commit('addToStore', { prop: 'languages', data: languages });
  }
}